package com.jenkins.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jenkins.model.Employee;

@RestController
public class EmployeeController {
	
	@RequestMapping("/employee")
    public List<Employee> getEmployees() 
    {
		List<Employee> employeesList = new ArrayList<Employee>();
		employeesList.add(new Employee(1,"Ravi","Kumar","ravi@gmail.com"));
		employeesList.add(new Employee(1,"Raj","Kumar","raj@gmail.com"));
		employeesList.add(new Employee(1,"Ramesh","Kumar","ramesh@gmail.com"));
		return employeesList;
    }

}

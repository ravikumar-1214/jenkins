package com.jenkins.test;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;



public class JavaDateValidations 
{
	public static void main(String[] args) 
    {
      
        System.out.println("isValid - yyyy-MM-dd HH:MM:SS with 2012-07-10 14:58:00 = " + isValidIssueTime("2012-07-10 14:58:00"));
        
        System.out.println("isValid - yyyy-MM-dd HH:MM:SS with 2012-07-10 14:58:00 = " + isValidIssueTime("10-07-2012 14:58:00"));
        
        System.out.println("isValid - yyyy-MM-dd HH:MM:SS with 2012-07-10 14:58:00 = " + checkIssuerTimeFormat("10-07-2012 14:58:00"));
     
    }
     
   
    
	public static boolean isValidIssueTime(String issueTime) {
		String format = "yyyy-MM-dd HH:mm:ss";
		LocalDateTime localDateTime = null;
		DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, Locale.ENGLISH);
		try {
			localDateTime = LocalDateTime.parse(issueTime, fomatter);
			String result = localDateTime.format(fomatter);
			return result.equals(issueTime);
		}catch (DateTimeParseException e) {
		}
		return false;
	}
	
	
	public static boolean checkIssuerTimeFormat(String issuerTime)  {

		try {
			java.util.Date temp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
			        .parse(issuerTime);
			if(temp !=null) {
				return true;
			}else {
				return false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
   }
}